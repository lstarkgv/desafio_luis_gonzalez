const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let transferenciaSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    rut: {
        type: String,
        required: [true, 'El RUT es necesario']
    },
    banco: {
        type: String,
        required: [true, 'El banco es necesario']
    },
    monto: {
        type: Number,
        min: 1,
        required: [true, 'El monto es necesario y debe ser mayor a 0']
    },
    estado: {
        type: Boolean,
        default: true
    }
});

transferenciaSchema.methods.toJSON = function() {

    let transferencia = this;
    let transferenciaObject = transferencia.toObject();

    return transferenciaObject;
}

transferenciaSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Transferencia', transferenciaSchema);