# Desafío Tecnico - Luis González Villagra

Pieza de software que simula una transferencia con las siguientes funcionalidades:

 - Nuevo Destinatario
 - Transferencia
 - Historial

Como extra se agrego sistema de seguridad basado en token (JWT).

 * Credenciales:
  
   **usuario**: admin@banco.com
   **clave**: adminripley

# Arquitectura 

* El FrontEnd esta creado con Angular 12 e Ionic (para UI responsiva compatible con todas las pantallas y temas oscuros si este esta activo).

* El BackEnd es un API REST creado con NodeJS Express con el ORM Mongoose para conectar con MongoDB

* Base de datos MongoDB Cluster bajo MongoDB Atlas (SaaS en Google Cloud Platform).

* Infraestructura montada en GCP y desplegado con Docker en servidor Ubuntu Server.

**Repositorio Backend:** https://gitlab.com/lstarkgv/desafio_luis_gonzalez

**Repositorio Frontend:** https://gitlab.com/lstarkgv/desa_luis_gonzalez_front

![arquitectura](arquitectura.png)

# API

Method **POST**

* Create User

**EndPoint: {apiURL}/api/usuario**

Bodyraw (json)

`{
  "nombre": "admin",
  "email": "admin@banco.com",
  "password": "adminripley",
  "role": "ADMIN_ROLE"
}`

---

Method **POST**

* Login

**EndPoint: {apiURL}/api/login**

Bodyraw (json)
json

`{
  "email": "admin@banco.com",
  "password": "adminripley"
}`

---

Method **GET**

* Get Destinatarios

**EndPoint: {apiURL}/api/destinatario**

`Request Headers
Authorization: Token`

---

Method **POST**

* Save Destinatario

**EndPoint: {apiURL}/api/destinatario**

`Request Headers
Authorization: token`


Bodyraw (json)

`{
  "nombre": "Luis Gonzalez",
  "rut": "11111111-1",
  "correo": "luis@banco.com",
  "telefono": "+56911111111",
  "banco": "Banco Ejemplo",
  "tipo_cuenta": "Cuenta Corriente",
  "num_cuenta": "123456789"
}`

---

Method **POST**

* Save Transferencia

**EndPoint: {apiURL}/api/transferencia**

`Request Headers
Authorization: token`

Bodyraw (json)

`{
  "nombre": "Luis",
  "rut": "11111111-1",
  "banco": "Banco Ejemplo",
  "tipo_cuenta": "Cuenta Corriente",
  "monto": 100
}`

---

Method **GET**

* Get Transferencia

**EndPoint: {apiURL}/api/transferencia**

`Request Headers
Authorization: token`