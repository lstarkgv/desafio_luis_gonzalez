FROM node:8.11-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

ENV host 'localhost'
ENV db 'mongodb+srv://ripluser01:ripluser01@cluster0.jfcgg.gcp.mongodb.net/desafio?retryWrites=true&w=majority'
ENV seed 'rplinnova'
ENV caducidad_token '48h'
ENV PORT 3033

EXPOSE $PORT
CMD [ "npm", "start" ]
