const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');

const Transferencia = require('../models/transferencia');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

const app = express();

//Listar transferencias
app.get('/transferencia', verificaToken, (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);

    Transferencia.find({ estado: true }, 'nombre rut banco tipo_cuenta monto')
        .skip(desde)
        .limit(limite)
        .exec((err, transferencias) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Transferencia.count({ estado: true }, (err, conteo) => {

                res.json({
                    ok: true,
                    transferencias,
                    cuantos: conteo
                });
            });
            
        });

});

//Nueva transferencia
app.post('/transferencia', verificaToken,  (req, res) => {

    let body = req.body;

    let transferencia = new Transferencia({
        nombre: body.nombre,
        rut: body.rut,
        banco: body.banco,
        tipo_cuenta: body.tipo_cuenta,
        monto: body.monto
    });

    transferencia.save((err, transferenciaDB) => {
        if (err) {
            console.log(err);
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            usuario: transferenciaDB
        });
    });

});

module.exports = app;