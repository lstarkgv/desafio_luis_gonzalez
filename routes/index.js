var express = require('express');
var app = express();

app.use(require('./login'));
app.use(require('./usuario'));
app.use(require('./destinatario'));
app.use(require('./transferencia'));

module.exports = app;
