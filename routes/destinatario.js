const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');

const Destinatario = require('../models/destinatario');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

const app = express();

//Listar destintarios
app.get('/destinatario', verificaToken, (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);

    Destinatario.find({ estado: true }, 'nombre RUT correo banco tipo_cuenta num_cuenta')
        .skip(desde)
        .limit(limite)
        .exec((err, destinatarios) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Destinatario.count({ estado: true }, (err, conteo) => {

                res.json({
                    ok: true,
                    destinatarios,
                    cuantos: conteo
                });

            });

        });

});

//Buscar destinatario por Rut
app.get('/destinatario/:rut', verificaToken, (req, res) => {

    let rut = req.params.rut;

    Destinatario.find({ rut : rut })
        .exec((err, destinatario) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                destinatario
            })

        })

});

//Nuevo destinatario
app.post('/destinatario', verificaToken,  (req, res) => {

    let body = req.body;

    console.log(body);

    let destinatario = new Destinatario({
        nombre: body.nombre,
        rut: body.rut,
        correo: body.correo,
        telefono: body.telefono,
        banco: body.banco,
        tipo_cuenta: body.tipo_cuenta,
        num_cuenta: body.num_cuenta
    });


    destinatario.save((err, destinatarioDB) => {

        if (err) {
            console.log(err);
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            usuario: destinatarioDB
        });


    });


});

module.exports = app;