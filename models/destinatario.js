const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let destinatarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    rut: {
        type: String,
        unique: true,
        required: [true, 'El RUT es necesario']
    },
    correo: {
        type: String,
        required: [true, 'El correo es necesario']
    },
    telefono: {
        type: String,
        required: [true, 'El telefono es necesario']
    },
    banco: {
        type: String,
        required: [true, 'El banco es necesario']
    },
    tipo_cuenta: {
        type: String,
        required: [true, 'El tipo de cuenta es necesario']
    },
    num_cuenta: {
        type: String,
        required: [true, 'El numero de cuenta es necesario']
    },
    estado: {
        type: Boolean,
        default: true
    }
});

destinatarioSchema.methods.toJSON = function() {

    let destinatario = this;
    let destinatarioObject = destinatario.toObject();

    return destinatarioObject;
}

destinatarioSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Destinatario', destinatarioSchema);